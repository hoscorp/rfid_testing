#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN 8
#define RST_PIN 5

MFRC522 mfrc522(SS_PIN, RST_PIN);

void setup() 
{
    Serial.begin(9600);

    SPI.begin();
    mfrc522.PCD_Init();
    SPI.setClockDivider(SPI_CLOCK_DIV32);

    Serial.println(F("Awake"));
}

void loop() 
{
    if(!mfrc522.PICC_IsNewCardPresent()) {
        delay(500);
        return;
    }

    if(!mfrc522.PICC_ReadCardSerial())
        return;

    byte uidchar[sizeof(mfrc522.uid)];
    memcpy(uidchar, &(mfrc522.uid), sizeof(mfrc522.uid));

    Serial.print(F("UID:"));
    for (byte i = 1; i < 5; i++) {
        Serial.print(uidchar[i] < 0x10 ? " 0" : " ");
        Serial.print(uidchar[i], HEX);
    }

    Serial.println();
}
